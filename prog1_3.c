// two command line arg called FILE and SECTION_NAME.
// FILE will be target elf file.
// SECTION_NAME will be a string identifying which sections 
// for which your program will produce crc32.
// If multiple sections match the SECTION_NAME, prouce crc32 for each of them

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned int crc32(unsigned char* message, long length){
    int i, j;
    unsigned int byte, crc, mask;

    i = 0;
    crc = 0xFFFFFFFF;
    while(i < length) {
        byte = message[i];
        crc = crc ^ byte;
        for(j = 7; j >= 0; j--){
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
        }
        i++;
    }
    return ~crc;
}

int main(int argc,char* argv[]){
    printf("Assignment#1-3, Travis Dattilo, travisdattilo@yahoo.com\n");
    if(argc != 3){
        printf("\nThis program expects two command line arguments.\n"); //Error message
        return 0;
    }
    
    FILE* inputFile; 
    char* buffer;
    char* SECTION_NAME = argv[2];
    long length; 
    int checksum;

    inputFile = fopen(argv[1],"rb");
    fseek(inputFile,0,SEEK_END);
    length = ftell(inputFile);
    fseek(inputFile, 0, SEEK_SET);

    buffer = (char*)malloc(length); 
    
    fread(buffer, length, 1, inputFile);

    long offset; 
    short entrySize; 
    short entryCount; 
    long headerSize;
    short strTableIndex; 

    if(buffer[0x4] == 2){   // 64 Bit  
        offset = *(long*)(buffer + 0x28);           // the start of the section header table from the start of the file   
        entrySize = *(short*)(buffer + 0x3A);       // how large each section header is 
        entryCount = *(short*)(buffer + 0x3C);      // how many section headers are ther e
        strTableIndex = *(short*)(buffer + 0x3E);   // index of section header that points to the string name
    }   
    else if(buffer[0x4] == 1){  // 32 Bit
        offset = *(int*)(buffer + 0x20);
        entrySize = *(short*)(buffer + 0x2E);
        entryCount = *(short*)(buffer + 0x30);
        strTableIndex = *(short*)(buffer + 0x32);
    }
    else{
        printf("Not 32 or 64 bit");
        exit(0);
    }

    char* sectionBuffer = buffer + offset;      // how deep into the file to go, the pointer to the first section
    char* strTableSection = sectionBuffer + (entrySize * strTableIndex);   // depending on StrTableIndex, this is the current section header
    long offsetStrTableNames;

    if(buffer[0x4] == 2){   // 64 Bit
        offsetStrTableNames = *(long*)(strTableSection + 0x18);     
    }
    else{   // 32 Bit
        offsetStrTableNames = *(int*)(strTableSection + 0x10);  
    }   

    
    char* names = buffer + offsetStrTableNames; // where all strings are at 
    int nameOffset;
    char* name;   


    for(int i = 0; i < entryCount; i++,sectionBuffer+=entrySize){  // incrementing buffer to next section and incrementing i
        nameOffset = *(int*) sectionBuffer;
        name = names + nameOffset;  
        //printf("name: %s\n",name);    
        if(strcmp(name,SECTION_NAME) == 0){
            if(entrySize > 0){
                checksum = crc32(sectionBuffer,entrySize);  
                printf("%08x\n", checksum);                 
            }
            else{
                printf("This section is empty");
            }
        }   
    }

    fclose(inputFile); 
    
    return 0;    

}









