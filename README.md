Travis Dattilo  
travisdattilo@yahoo.com  

"prog1_1.c"  
Compile and run this C program with one command line argument that is a target ELF file.
The program will read the command line argument as a binary file and calculate the crc32 
checksum of the whole file. It will then print it to standard out as an 8 digit hexidecimal number.

"prog1_2.c"  
Compile and run this C program with one command line argument that is a target ELF file.
The program will read the command line argument as a binary file and calculate the crc32 
checksum of the file's program header table. It will then print it to standard out as an 
8 digit hexidecimal number.

"prog1_3.c"  
Compile and run this C program with two command line arguments: the first being a target 
ELF file, and the second being a section name from the file. The program will read the first
command line argument as a binary file, the second as a string, and calculate
the crc32 checksum of the file's section name that matches the second command line argument. 
It will then print it to standard out as an 8 digit hexidecimal number. If there are multiple 
sections that have the same section name, the crc32 checksum will be printed for each of them.


