// single command line arg called FILE 
// FILE will be target elf file 
// read the whole thing as a binary file 
// print the file's PROGRAM HEADER TABLE CRC32 checksum as 8 digit hex number 

#include <stdio.h>
#include <stdlib.h>

unsigned int crc32(unsigned char* message, long length){
    int i, j;
    unsigned int byte, crc, mask;

    i = 0;
    crc = 0xFFFFFFFF;
    while(i < length) {
        byte = message[i];
        crc = crc ^ byte;
        for(j = 7; j >= 0; j--){
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
        }
        i++;
    }
    return ~crc;
}

int main(int argc,char* argv[]){
    printf("Assignment#1-2, Travis Dattilo, travisdattilo@yahoo.com\n");
    if(argc != 2){
        printf("\nThis program expects a single command line argument.\n"); //Error message
        return 0;
    }
    
    FILE* inputFile; 
    char* buffer;
    long length; 
    int checksum;

    inputFile = fopen(argv[1],"rb");
    fseek(inputFile,0,SEEK_END);
    length = ftell(inputFile);
    fseek(inputFile, 0, SEEK_SET);

    buffer = (char*)malloc(length); 
    
    fread(buffer, length, 1, inputFile);

    long offset; 
    short entrySize; 
    short entryCount; 
    long headerSize;

    if(buffer[0x4] == 2){   // 64 Bit  
        offset = *(long*)(buffer+0x20);         // points to the start of the program header table
        entrySize = *(short*)(buffer+0x36);     // the size of a program header table entry
        entryCount = *(short*)(buffer+0x38);    // the number of entries in the section header table
    }   
    else if(buffer[0x4] == 1){  // 32 Bit
        offset = *(int*)(buffer+0x1C);
        entrySize = *(short*)(buffer+0x2A);
        entryCount = *(short*)(buffer+0x2C);
    }
    else{
        printf("Not 32 or 64 bit");
        exit(0);
    }

    headerSize = entrySize * entryCount;
    
    fclose(inputFile); 
    
    checksum = crc32(buffer+offset,headerSize); 

    printf("%08x\n", checksum); 
    return 0;    

}





















