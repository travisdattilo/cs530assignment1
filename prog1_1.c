// single command line arg called FILE 
// FILE will be target elf file 
// read the whole thing as a binary file 
// print the file's CRC32 checksum as 8 digit hex number 

#include <stdio.h>
#include <stdlib.h>

unsigned int crc32(unsigned char* message, long length){
    int i, j;
    unsigned int byte, crc, mask;

    i = 0;
    crc = 0xFFFFFFFF;
    while(i < length) {
        byte = message[i];
        crc = crc ^ byte;
        for(j = 7; j >= 0; j--){
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
        }
        i++;
    }
    return ~crc;
}

int main(int argc,char* argv[]){
    printf("Assignment#1-1, Travis Dattilo, travisdattilo@yahoo.com\n");
    if(argc != 2){
        printf("\nThis program expects a single command line argument.\n"); //Error message
        return 0;
    }
    
    FILE* inputFile; 
    char* buffer;
    long length; 
    int checksum;

    inputFile = fopen(argv[1],"rb");
    fseek(inputFile,0,SEEK_END);
    length = ftell(inputFile);
    fseek(inputFile, 0, SEEK_SET);

    buffer = (char*)malloc(length); 
    
    fread(buffer, length, 1, inputFile);
    fclose(inputFile); 
    
    checksum = crc32(buffer,length); 

    printf("%08x\n", checksum); 
    return 0;    

}














